# Task:
# count the number of times a depth measurement increases from the previous measurement.

# Idea:
# Read values from file and put them in a list
# A counter that keeps track of how many times the depth has increased
# A var that saves prev value
# A loop that checks current value against prev and adds to the counter if larger
# Save current value as prev before next iteration

depths = [int(i) for i in open("datafiles/depthsData.txt").readlines()]

timesDepthIncreased = 0

prevDepth = depths[0]

for depth in depths[1:]:
    if depth > prevDepth:
        timesDepthIncreased += 1
    prevDepth = depth

print("Depth has increased " + str(timesDepthIncreased) + " times")

# correct answer = 1696
