# Task:
# 1. Calculate the horizontal position and depth you would have after following the planned course.
# 2. What do you get if you multiply your final horizontal position by your final depth?

# Idea:
# read datafile to list
# have one var for horizontal movement and one for depth. Both start at 0
# loop through the data list and check if horizontal or depth movement, add accordingly
# calculate the final position by multiplying horizontal and depth vars

from collections import defaultdict

# kan man göra detta bättre med en dictionary som läser in paren av movement plus antal steg direkt?
course = open("datafiles/positionData.txt").readlines()

horizontalMovement = 0
depthMovement = 0
aim = 0

for position in course:
    currentMovement = position.split(" ")
    if currentMovement[0] == "forward":
        i = int(currentMovement[1])
        horizontalMovement += i
        depthMovement += aim * i
    elif currentMovement[0] == "down":
        aim += int(currentMovement[1])
    else:
        aim -= int(currentMovement[1])
print('horizontal 1: ' + str(horizontalMovement))
print('depth 1: ' + str(depthMovement))
print(horizontalMovement * depthMovement)

# correct answer: 1685186100